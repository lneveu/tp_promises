/* see https://jsfiddle.net/vooor/b76peurk/ */

const api = {
	dataSource		: "https://bitbucket.org/lneveu/tp_promises/raw/master/data/sources.json"
	,getImageUrl	: name => new Promise( resolve => setTimeout( () => resolve(`https://bitbucket.org/lneveu/tp_promises/raw/master/img/${name}.png` ),Math.floor(Math.random()*1e3) ) )
	,displayImages	: images =>
	{
		images.forEach( (image,i) =>
		{
			let img = document.createElement("img");
			img.src = image;
			img.style.position = "absolute";
			img.style.top = "0px";
			img.style.left = "0px";
			i>0 && setTimeout( () => { img.className = "force"},Math.floor(Math.random()*5e3));
			document.body.appendChild(img);
		});
		return Promise.resolve('ok');
	}
};
